package Libreria;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int opcion;
		Clase1 metodos1 = new Clase1();
		Clase2 metodos2 = new Clase2();
		opcion=menu();
		switch(opcion) {
		case 1:
			System.out.println("Numero = ");
			int n=in.nextInt();
			System.out.println("Numero = ");
			int n2=in.nextInt();
			metodos1.calcularResta(n, n2);
			break;
		case 2:
			System.out.println("Numero = ");
			n=in.nextInt();
			System.out.println("Numero = ");
			n2=in.nextInt();
			metodos2.calcularDivision(n,n2);
			break;
		case 3:
			String cadena;
			System.out.println("Introduce una cadena = ");
			cadena=in.nextLine();
			metodos2.cadenaInversa(cadena);
			break;
		case 4:
			System.out.println("Introduce una cadena = ");
			cadena=in.nextLine();
			metodos2.cadenaMayusculas(cadena);
			break;
		case 5:
			System.out.println("Base = ");
			int base=in.nextInt();
			System.out.println("Exponente = ");
			int exponente=in.nextInt();
			int potencia=metodos2.pow(base, exponente);
			System.out.println("La potencia es " + potencia);
			break;
		case 6:
			metodos1.calcularSuma();
			break;
		case 7:
			System.out.println("Que numero quieres calcular");
			n=in.nextInt();
			int valor;
			valor=metodos1.calcularPrimos(n);
			if(valor==2)
				System.out.println("El numero es primo");
			else
				System.out.println("El numero NO es primo");
			break;
		case 8:
			System.out.println("Numero = ");
			n=in.nextInt();
			metodos1.calcularPiramide(n);
			break;
		case 9:
			System.out.println("Numero = ");
			n=in.nextInt();
			metodos1.calcularFactorial(n);
		break;
		case 10:
			System.out.println("Numero = ");
			n=in.nextInt();
			System.out.println("Numero = ");
			n2=in.nextInt();
			metodos2.calcularMultiplicacion(n,n2);
		break;
		}
		
			
		
		
	}

	private static int menu() {
		Scanner in = new Scanner(System.in);
		int opcion;
		System.out.println("*** MENU ***");
		System.out.println("1. Calcular resta");
		System.out.println("2. Calcular division");
		System.out.println("3. Invertir cadena");
		System.out.println("4. Cadena en mayusculas");
		System.out.println("5. Calcular potencia");
		System.out.println("6. Calcular suma");
		System.out.println("7. Calcular primos");
		System.out.println("8. Calcular piramide");
		System.out.println("9. Calcular factorial");
		System.out.println("10. Calcular multiplicacion");
		return opcion=in.nextInt();
	}


}
