﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        public class Main
        {

            public static void main(String[] args)
            {
                int opcion;
                opcion = menu();
                switch (opcion)
                {
                    case 1:
                        System.Console.WriteLine("Numero = ");
                        int n = int.Parse(Console.ReadLine());
                        System.Console.WriteLine("Numero = ");
                        int n2 = int.Parse(Console.ReadLine());
                        ClassLibrary2.Class1.calcularResta();
                        break;
                    case 2:
                        System.Console.WriteLine("Numero = ");
                        n = int.Parse(Console.ReadLine());
                        System.Console.WriteLine("Numero = ");
                        n2 = int.Parse(Console.ReadLine());
                        ClassLibrary3.Class1.CalcularDivision(int n);
                        break;
                    case 3:
                        String cadena;
                        System.Console.WriteLine("Introduce una cadena = ");
                        cadena = Console.ReadLine();
                        ClassLibrary3.Class1.cadenaInversa();
                        break;
                    case 4:
                        System.Console.WriteLine("Introduce una cadena = ");
                        cadena = Console.ReadLine();
                        ClassLibrary2.Class1.cadenaMayusculas();
                        break;
                    case 5:
                        System.Console.WriteLine("Base = ");
                        int basee = int.Parse(Console.ReadLine());
                        System.Console.WriteLine("Exponente = ");
                        int exponente = int.Parse(Console.ReadLine());
                        int potencia = ClassLibrary3.Class1.pow();
                        System.Console.WriteLine("La potencia es " + potencia);
                        break;
                    case 6:
                        ClassLibrary2.Class1.calcularSuma();
                        break;
                    case 7:
                        System.Console.WriteLine("Que numero quieres calcular");
                        n = int.Parse(Console.ReadLine());
                        int valor;
                        valor = ClassLibrary3.Class1.calcularPrimos();
                        if (valor == 2)
                            System.Console.WriteLine("El numero es primo");
                        else
                            System.Console.WriteLine("El numero NO es primo");
                        break;
                    case 8:
                        System.Console.WriteLine("Numero = ");
                        n = int.Parse(Console.ReadLine());
                        ClassLibrary3.Class1.cadenaPiramide();
                        break;
                    case 9:
                        System.Console.WriteLine("Numero = ");
                        n = int.Parse(Console.ReadLine());
                        ClassLibrary3.Class1.calcularFactorial(n);
                        break;
                    case 10:
                        System.Console.WriteLine("Numero = ");
                        n = int.Parse(Console.ReadLine());
                        System.Console.WriteLine("Numero = ");
                        n2 = int.Parse(Console.ReadLine());
                        ClassLibrary3.Class1.calcularMultiplicacion(n, n2);
                        break;
                }




            }

            private static int menu()
            {
                int opcion;
                System.Console.WriteLine("*** MENU ***");
                System.Console.WriteLine("1. Calcular resta");
                System.Console.WriteLine("2. Calcular division");
                System.Console.WriteLine("3. Invertir cadena");
                System.Console.WriteLine("4. Cadena en mayusculas");
                System.Console.WriteLine("5. Calcular potencia");
                System.Console.WriteLine("6. Calcular suma");
                System.Console.WriteLine("7. Calcular primos");
                System.Console.WriteLine("8. Calcular piramide");
                System.Console.WriteLine("9. Calcular factorial");
                System.Console.WriteLine("10. Calcular multiplicacion");
                return opcion = int.Parse(Console.ReadLine());
            }


        }

    }
}
