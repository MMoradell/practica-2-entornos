package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JSpinner;
import java.awt.Color;

/**
 * 
 * @author Marcos
 * @since 13-12-2017
 */

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField txtInformacinPersonal;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/Imagen/descarga.jpg")));
		setTitle("Mi ventana");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 508, 343);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		menuBar.add(mnArchivo);
		
		JMenuItem mntmAbrir = new JMenuItem("Abrir");
		mnArchivo.add(mntmAbrir);
		
		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);
		
		JMenu mnSalir = new JMenu("Salir");
		mnArchivo.add(mnSalir);
		
		JMenu mnEdicin = new JMenu("Edici\u00F3n");
		menuBar.add(mnEdicin);
		
		JMenuItem mntmDeshacer = new JMenuItem("Deshacer");
		mnEdicin.add(mntmDeshacer);
		
		JMenuItem mntmRehacer = new JMenuItem("Rehacer");
		mnEdicin.add(mntmRehacer);
		
		JMenuItem mntmCopiar = new JMenuItem("Copiar");
		mnEdicin.add(mntmCopiar);
		
		JMenuItem mntmPegar = new JMenuItem("Pegar");
		mnEdicin.add(mntmPegar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmIdioma = new JMenuItem("Idioma");
		mnAyuda.add(mntmIdioma);
		
		JMenuItem mntmContenidos = new JMenuItem("Contenidos");
		mnAyuda.add(mntmContenidos);
		
		JMenuItem mntmInformacinDelPrograma = new JMenuItem("Informaci\u00F3n del programa");
		mnAyuda.add(mntmInformacinDelPrograma);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtInformacinPersonal = new JTextField();
		txtInformacinPersonal.setBounds(5, 5, 422, 22);
		txtInformacinPersonal.setText("INFORMACI\u00D3N PERSONAL");
		contentPane.add(txtInformacinPersonal);
		txtInformacinPersonal.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(84, 53, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(15, 50, 71, 25);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(12, 88, 56, 16);
		contentPane.add(lblApellidos);
		
		textField_1 = new JTextField();
		textField_1.setBounds(84, 85, 116, 22);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblDni = new JLabel("DNI");
		lblDni.setBounds(15, 123, 56, 16);
		contentPane.add(lblDni);
		
		textField_2 = new JTextField();
		textField_2.setBounds(84, 120, 116, 22);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Casadx", "Viudx", "Solterx"}));
		comboBox.setBounds(15, 196, 88, 22);
		contentPane.add(comboBox);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		rdbtnHombre.setBounds(124, 195, 88, 25);
		contentPane.add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		rdbtnMujer.setBounds(222, 195, 71, 25);
		contentPane.add(rdbtnMujer);
		
		JRadioButton rdbtnOtro = new JRadioButton("Otro");
		rdbtnOtro.setBounds(313, 195, 63, 25);
		contentPane.add(rdbtnOtro);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(Ventana.class.getResource("/Imagen/30427.png")));
		lblNewLabel.setBounds(344, 36, 134, 121);
		contentPane.add(lblNewLabel);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(313, 85, 30, 22);
		contentPane.add(spinner);
		
		JLabel lblNDeHijos = new JLabel("N\u00BA de hijos");
		lblNDeHijos.setBounds(240, 88, 71, 16);
		contentPane.add(lblNDeHijos);
	}
}
