package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i <= cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			// El error se produce en el "i <= cadenaLeida.length()", 
			// ya que deberia ser "i < cadenaLeida.length(0)" porque el .length 
			// cuenta el primer caracter con como si fuese el 0, por lo tanto si
			// introducimos una cadena de 4 caracteres, leera 0 1 2 y 3, por lo que
			// al no llegar al 4 muestra el error 
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
